# flask-mega-tut

Flask is not included in the Repo to install:

 1. `git clone` the repo first
 2. `cd flask-mega-tut`
 3. `sudo apt-get install python-virtualenv`
 4. `virtualenv flask`
  
Install extensions:
    
    flask/bin/pip install flask
    flask/bin/pip install flask-login
    flask/bin/pip install flask-openid
    flask/bin/pip install flask-mail
    flask/bin/pip install flask-sqlalchemy
    flask/bin/pip install sqlalchemy-migrate
    flask/bin/pip install flask-whooshalchemy
    flask/bin/pip install flask-wtf
    flask/bin/pip install flask-babel
    flask/bin/pip install guess_language
    flask/bin/pip install flipflop
    flask/bin/pip install coverage

To run: 
`./run.py`

Create the db

    sudo chmod a+x create_db.py
    ./create_db.py

Make other helper script executable

    sudo chmod a+x db_upgrade.py
    sudo chmod a+x db_downgrade.py
    sudo chmod a+x db_migrate.py